package com.qiu.scala

import java.util.Date

/**
 * @Author: qiu
 * @Time: 2022/11/14 20:01:09
 * @Description:
 */
object FunctionDemo {
  def main(args: Array[String]): Unit = {
    val a: Int = 100 // 定义一个常量
    var b: String = "hello" // 定义一个变量

    println("=========定义函数==========")
    def fun(a: Int, b: String): Unit = {
      println(s"$a, $b")
    }
    // 方法如果不想执行，可以赋给一个引用，方法名 + 空格 + 下划线
    val f = fun _

    println("=========匿名函数==========")
    // 函数是第一类值
    val fun01: (Int, Int) => Int = (a: Int, b: Int) => {
      a + b
    }

    println("=========默认值函数==========")

    def fun02(a: String = "hello", b: String = "world", c: String): Unit = {
      println(s"$a, $b, $c")
    }

    def fun03(a: String, b: String): Unit = {
      println(s"$a, $b")
    }

    fun02(c = "cc") // 没有提供默认值的显示给出
    fun02(b = "b", c = "c", a = "a") // 顺序可以不同

    fun03("aa", b = "bb")
    fun03(b = "bb", a = "aa") // 顺序可以不同

    println("=========递归函数==========")

    // 计算n的阶乘
    def fun04(n: Int): Int = {
      if (n == 1) {
        return 1
      }
      n * fun04(n - 1)
    }

    println(fun04(3))

    println("=========嵌套函数==========")

    // 以一个打印日志的方法为例说明
    def fun05(tp: String, date: Date, msg: String): Unit = {
      println(s"$tp: $date-$msg")
    }

    fun05("info", new Date(), "This is a log")
    // 定义偏应用函数
    val info = fun05("info", _: Date, _: String)
    val error = fun05("error", _: Date, _: String)
    info(new Date(), "This is a log")
    error(new Date(), "This is a log")

    println("=========高阶函数==========")

    // 函数是第一类值，可以作为参数和返回值
    def fun06(a: Int, b: Int, f: (Int, Int) => Int): Unit = {
      val r = f(a, b)
      println(r)
    }
    fun06(100, 200, (a: Int, b: Int) => {
      a + b
    })

    def fun07(op: String): (Int, Int) => Double = {
      op match {
        case "+" => (a: Int, b: Int) => a + b
        case "-" => (a: Int, b: Int) => a - b
        case "*" => (a: Int, b: Int) => a * b
        case "/" => (a: Int, b: Int) => a.toDouble / b
      }
    }

    val plus = fun07("/")
    println(plus(100, 200))

    println("=========柯里化==========")
    def fun08(a: Int*)(b: String): Unit = {
      println(s"$a, $b")
    }

    fun08(100, 200)("Hello")

  }
}
